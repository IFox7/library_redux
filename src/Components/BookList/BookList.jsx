import React from "react";
import { useState } from "react";

import "./bookList.css";
import { useDispatch, useSelector } from "react-redux";
import BtnPagination from "../BtnPagination/BtnPagination";

import badImage from "../../Images/wrongImage1.jpg";
import { actionDeleteBook } from "../../Store/Reducer/Reducer";
import BookCardModal from "../BookCardModal/BookCardModal";
import { FetchRequest } from "../../FetchRequest/FetchReuest";

import { motion } from "framer-motion";
const hiddenMask = `repeating-linear-gradient(to right, rgba(0,0,0,0) 0px, rgba(0,0,0,0) 30px, rgba(0,0,0,1) 30px, rgba(0,0,0,1) 30px)`;
const visibleMask = `repeating-linear-gradient(to right, rgba(0,0,0,0) 0px, rgba(0,0,0,0) 0px, rgba(0,0,0,1) 0px, rgba(0,0,0,1) 30px)`;
// Відображення сторінки з 40 книжок з можливістю пагінації
export default function BookList() {
  // Стейти для анімації елементів
  const [isLoaded, setIsLoaded] = useState(false),
    [isInView, setIsInView] = useState(false);
  // стейти reducer і управління мод.вікнами
  const allBooks = useSelector((state) => state.booksArr),
    dispatch = useDispatch(),
    [show, setShow] = useState(false),
    [modalCard, setModalCard] = useState({});
  return (
    <>
      {/* Очистка фільтрів */}
      <div className="btnFilterClearWrapper">
        <button
          onClick={() => dispatch(FetchRequest(0, 0))}
          className={
            allBooks?.length === 0 || allBooks?.length > 38
              ? "btnNavbar visibilFilter"
              : "btnNavbar"
          }
          type="button"
        >
          Очистити ❌ фільтр
        </button>
      </div>
      {/* Відображення карток з книгами на сторінці */}
      <div className="bookCardsWrapper">
        {allBooks?.length < 10
          ? allBooks?.map((elem) => {
              return elem.volumeInfo.authors ? (
                <motion.div
                  key={elem.id}
                  className="cardWrapper"
                  initial={false}
                  animate={
                    isLoaded && isInView
                      ? { WebkitMaskImage: visibleMask, maskImage: visibleMask }
                      : { WebkitMaskImage: hiddenMask, maskImage: hiddenMask }
                  }
                  transition={{ duration: 1, delay: 0 }}
                  viewport={{ once: true }}
                  onViewportEnter={() => setIsInView(true)}
                  onLoad={() => setIsLoaded(true)}
                >
                  {/* Кнопка видалення книжки з сторінки */}
                  <div
                    className="deleteCard"
                    onClick={() => {
                      return dispatch(actionDeleteBook(elem.id));
                    }}
                  >
                    💥
                  </div>
                  <div className="imgCardWrapper">
                    <img
                      // Наповнення інфо для модального вікна
                      onClick={() => {
                        setModalCard({
                          imgSrc: elem.volumeInfo.imageLinks?.thumbnail
                            ? elem.volumeInfo.imageLinks?.thumbnail
                            : badImage,
                          title: elem.volumeInfo.title,
                          authors: elem.volumeInfo.authors,
                          language: elem.volumeInfo.language,
                          descript: elem.volumeInfo.description,
                          id: elem.id,
                          preview: elem.volumeInfo.previewLink,
                          isbn: elem.volumeInfo.industryIdentifiers
                            ? elem.volumeInfo.industryIdentifiers[0].identifier
                            : "Unknown",
                          date: elem.volumeInfo.publishedDate,
                        });
                        setShow(true);
                      }}
                      src={
                        elem.volumeInfo.imageLinks?.thumbnail
                          ? elem.volumeInfo.imageLinks?.thumbnail
                          : badImage
                      }
                      alt="bookImage"
                    />
                  </div>
                  <div className="cardAboutWrapper">
                    <div className="aboutBook">{elem.volumeInfo.title}</div>
                    <div className="aboutAuthors">
                      <span className="authors">AUTHORS: </span>
                      {elem.volumeInfo.authors !== undefined
                        ? elem.volumeInfo.authors.map((elem, index) => (
                            <span key={index}>{elem}</span>
                          ))
                        : " Unknowns "}
                    </div>
                  </div>
                </motion.div>
              ) : null;
            })
          : allBooks?.map((elem) => {
              return (
                <motion.div
                  key={elem.id}
                  className="cardWrapper"
                  initial={false}
                  animate={
                    isLoaded && isInView
                      ? { WebkitMaskImage: visibleMask, maskImage: visibleMask }
                      : { WebkitMaskImage: hiddenMask, maskImage: hiddenMask }
                  }
                  transition={{ duration: 1, delay: 0 }}
                  viewport={{ once: true }}
                  onViewportEnter={() => setIsInView(true)}
                  onLoad={() => setIsLoaded(true)}
                >
                  <div
                    className="deleteCard"
                    onClick={() => {
                      return dispatch(actionDeleteBook(elem.id));
                    }}
                  >
                    💥
                  </div>
                  <div className="imgCardWrapper">
                    <img
                      onClick={() => {
                        setModalCard({
                          imgSrc: elem.volumeInfo.imageLinks?.thumbnail
                            ? elem.volumeInfo.imageLinks?.thumbnail
                            : badImage,
                          title: elem.volumeInfo.title,
                          authors: elem.volumeInfo.authors,
                          language: elem.volumeInfo.language,
                          descript: elem.volumeInfo.description,
                          id: elem.id,
                          preview: elem.volumeInfo.previewLink,
                          isbn: elem.volumeInfo.industryIdentifiers
                            ? elem.volumeInfo.industryIdentifiers[0].identifier
                            : "Unknown",
                          date: elem.volumeInfo.publishedDate,
                        });
                        setShow(true);
                      }}
                      src={
                        elem.volumeInfo.imageLinks?.thumbnail
                          ? elem.volumeInfo.imageLinks?.thumbnail
                          : badImage
                      }
                      alt="bookImage"
                    />
                  </div>
                  <div className="cardAboutWrapper">
                    <div className="aboutBook">{elem.volumeInfo.title}</div>
                    <div className="aboutAuthors">
                      <span className="authors">AUTHORS: </span>
                      {elem.volumeInfo.authors !== undefined
                        ? elem.volumeInfo.authors.map((elem, index) => (
                            <span key={index}>{elem}</span>
                          ))
                        : " Unknowns "}
                    </div>
                  </div>
                </motion.div>
              );
            })}
      </div>
      {/* Кнопки пагінації */}
      <BtnPagination
        visible={
          allBooks?.length > 0 && allBooks?.length > 30 ? "" : "visibility"
        }
      />
      {/* Модальне вікно з деталізованою інфо про книжку */}
      <BookCardModal show={show} setShow={setShow} modal={modalCard} />
    </>
  );
}

import React from "react";

import "./filterBooks.css";
import { useDispatch, useSelector } from "react-redux";
import { actionFilterBook } from "../../Store/Reducer/Reducer";

// Фільтрація книжок на сторінці за автором,жанром,роком
export default function FilterBooks() {
  const allBooks = useSelector((state) => state.booksArr),
    dispatch = useDispatch();

  return (
    <>
      <div className="filterWrapper">
        <div className="formFilter">Відфільтрувати літературу:</div>
        {/* Фільтр за автором */}
        <label className="author" htmlFor="check_author">
          за автором
        </label>
        <select
          onChange={(e) => {
            return dispatch(
              actionFilterBook({ value: e.target.value, name: e.target.name })
            );
          }}
          name="authors"
          id="check_author"
        >
          <option className="optionCheck" value="" checked>
            Автор :
          </option>
          {allBooks?.map((elem) => {
            return (
              <option key={elem.id} value={elem.volumeInfo.authors}>
                {elem.volumeInfo.authors}
              </option>
            );
          })}
        </select>
        {/* Фільр за жанром */}
        <label className="genre" htmlFor="check_genre">
          за жанром
        </label>
        <select
          onChange={(e) =>
            dispatch(
              actionFilterBook({ value: e.target.value, name: e.target.name })
            )
          }
          name="categories"
          id="check_genre"
        >
          <option className="optionCheck" value="" checked>
            Жанр :
          </option>
          {allBooks?.map((elem) => {
            return (
              <option key={elem.id} value={elem.volumeInfo.categories}>
                {elem.volumeInfo.categories}
              </option>
            );
          })}
        </select>
        {/* Фільтр за роком видання */}
        <label className="year" htmlFor="check_year">
          за роком видання
        </label>
        <select
          onChange={(e) =>
            dispatch(
              actionFilterBook({ value: e.target.value, name: e.target.name })
            )
          }
          name="publishedDate"
          id="check_year"
        >
          <option className="optionCheck" value="" checked>
            Рік видання :
          </option>
          {allBooks?.map((elem) => {
            return (
              <option key={elem.id} value={elem.volumeInfo.publishedDate}>
                {elem.volumeInfo.publishedDate}
              </option>
            );
          })}
        </select>
      </div>
    </>
  );
}

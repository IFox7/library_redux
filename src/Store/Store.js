import { createStore,applyMiddleware } from "redux";
import { Reducer } from "./Reducer/Reducer";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
// Стор проекту з переданим редюсером і міддлвеар з переданим thunk для роботи з асинхронною функцією
export const Store = createStore(Reducer,composeWithDevTools(applyMiddleware(thunk)));
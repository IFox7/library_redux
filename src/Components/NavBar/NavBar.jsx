import React from "react";

import "./index.css";
import "animate.css";

import logoImg from "../../Images/logo.jpg";
import { useDispatch, useSelector } from "react-redux";
import { FetchRequest } from "../../FetchRequest/FetchReuest";
import FavoritesBooks from "../FavoritesBooks/FavoritesBooks";
import { actionSearchBook, actionShow } from "../../Store/Reducer/Reducer";

// Навігаційна панель сторінки
export default function NavBar() {
  const dispatch = useDispatch(),
    allBooks = useSelector((state) => state.booksArr),
    favorites = useSelector((state) => state.favoritesBooksArr),
    reload = () => {
      window.location.reload();
    };

  return (
    <>
      <div className="navBarWrapper">
        <div onClick={reload} className="logoWrapper">
          <img src={logoImg} alt="logoImage" />
          <div className="logoName">Бібліотека онлайн</div>
        </div>
        <div className="btnNavbarWrapper">
          <button
            onClick={() => dispatch(FetchRequest(0, 0))}
            className={
              allBooks.length > 0
                ? "btnNavbar"
                : "btnNavbar animate__animated animate__pulse animate__infinite"
            }
            type="button"
          >
            Завантажити бібліотеку
          </button>
          <button
            onClick={() => dispatch(actionShow(true))}
            className={
              favorites.length === 0
                ? "btnNavbar"
                : "btnNavbar animate__animated animate__pulse animate__infinite"
            }
            type="button"
          >
            Збережена література
          </button>
        </div>
        <div className="searchNavbarWrapper">
          <input
            onChange={(e) => dispatch(actionSearchBook(e.target.value))}
            type="search"
            name="Book searcher"
            placeholder="Пошук по назві... 👁‍🗨"
            id="searchNavbar"
          />
        </div>
      </div>
      <FavoritesBooks />
    </>
  );
}

import React, { useEffect, useState } from "react";

import { motion } from "framer-motion";
import "./frontSlider.css";
import { useSelector } from "react-redux";

// Слайдер зображень книжок на головній сторінці,якщо бібліотека не завантажена з API
export default function FrontSlider() {
  const allBooks = useSelector((state) => state.booksArr);
  // Об'єкт 10 книжок для відображення в слайдері
  const ImagesSlider = {
    img1: "http://books.google.com/books/content?id=XAL2DwAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api",
    img2: "http://books.google.com/books/content?id=kzctDwAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api",
    img3: "http://books.google.com/books/content?id=mHhdgSkiJAIC&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api",
    img4: "http://books.google.com/books/content?id=3RZYEAAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api",
    img5: "http://books.google.com/books/content?id=JFPZrQEACAAJ&printsec=frontcover&img=1&zoom=1&source=gbs_api",
    img6: "http://books.google.com/books/content?id=SRadJIuhVjAC&printsec=frontcover&img=1&zoom=1&source=gbs_api",
    img7: "http://books.google.com/books/content?id=zSgZAQAAIAAJ&printsec=frontcover&img=1&zoom=1&source=gbs_api",
    img8: "http://books.google.com/books/content?id=U4Ciy8Hx0RIC&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api",
    img9: "http://books.google.com/books/content?id=mLjuAAAAMAAJ&printsec=frontcover&img=1&zoom=1&source=gbs_api",
    img10:
      "http://books.google.com/books/content?id=ElYEAAAAMBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api",
  };
  // Стейт для перебору зображень книжок слайдера
  const [count, setCount] = useState(0);
  // Запускаємо лічильник в інтервалі для динамічної зміни зображень в слайдері
  useEffect(() => {
    const interval = setInterval(() => {
      setCount((prev) => (prev < 9 ? prev + 1 : 0));
    }, 6000);
    return () => clearInterval(interval);
  }, []);

  return (
    <div
      className={allBooks?.length > 0 ? "boxWrapper invisible" : "boxWrapper"}
    >
      <motion.img
        initial={{ opacity: 0 }}
        className="box"
        animate={{
          scale: [1, 2, 2, 1, 1],
          rotate: [0, 0, 180, 180, 0],
          borderRadius: ["0%", "0%", "50%", "50%", "0%"],
          opacity: 1,
        }}
        transition={{
          duration: 5,
          ease: "easeInOut",
          times: [0, 0.2, 0.5, 0.8, 1],
          repeat: Infinity,
          repeatDelay: 1,
        }}
        src={Object.values(ImagesSlider)[count]}
      />
    </div>
  );
}

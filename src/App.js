import "bootstrap/dist/css/bootstrap.min.css";

import BookList from "./Components/BookList/BookList";
import NavBar from "./Components/NavBar/NavBar";
import FilterBooks from "./Components/FilterBooks/FilterBooks";
import FrontSlider from "./Components/FrontSlider/FrontSlider";
// Збираємо всі елементи проекту
function App() {
  return (
    <>
      <NavBar />
      <FilterBooks />
      <FrontSlider />
      <BookList />
    </>
  );
}

export default App;

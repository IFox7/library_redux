import React from "react";

import "./btnPagination.css";
import { FetchRequest } from "../../FetchRequest/FetchReuest";
import { useDispatch } from "react-redux";
// Кнопки пагінації 
export default function BtnPagination({ visible }) {
  const dispatch = useDispatch();
  return (
    <div
      className={
        visible !== ""
          ? "BtnPaginationWrapper visibility"
          : "BtnPaginationWrapper"
      }
    >
      <button
        className="btnPagination btnLast"
        onClick={() => dispatch(FetchRequest(0, -40))}
        type="button"
      >
        ПОПЕРЕДНЯ
      </button>
      <button
        className="btnPagination"
        onClick={() => dispatch(FetchRequest(0, 0))}
        type="button"
      >
        1
      </button>
      <button
        className="btnPagination"
        onClick={() => dispatch(FetchRequest(40, 0))}
        type="button"
      >
        2
      </button>
      <button
        className="btnPagination"
        onClick={() => dispatch(FetchRequest(80, 0))}
        type="button"
      >
        3
      </button>
      <button
        className="btnPagination btnLast"
        onClick={() => dispatch(FetchRequest(0, 40))}
        type="button"
      >
        НАСТУПНА
      </button>
    </div>
  );
}

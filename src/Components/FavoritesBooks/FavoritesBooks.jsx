import React from "react";

import { useDispatch, useSelector } from "react-redux";

import "./favoritesBooks.css";

import badImage from "../../Images/wrongImage1.jpg";
import Modal from "react-bootstrap/Modal";
import {
  actionDeleteFavoritesBook,
  actionShow,
} from "../../Store/Reducer/Reducer";
// Відображення книжок,що сподобались в окремому модальному вікні
function FavoritesBooks() {
  const dispatch = useDispatch();
  const favoritesAll = useSelector((state) => state.favoritesBooksArr);
  const show = useSelector((state) => state.show);
  return (
    <>
      <Modal
        show={show}
        fullscreen={true}
        onHide={() => dispatch(actionShow(false))}
      >
        <Modal.Header closeButton>
          <Modal.Title>Улюблена література</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {/* Масив улюблених книжок зі стейту reducer */}
          {favoritesAll.map((elem) => {
            return (
              <div key={elem.id} className="cardWrapper">
                <div
                  className="deleteCard"
                  onClick={() => {
                    return dispatch(actionDeleteFavoritesBook(elem.id));
                  }}
                >
                  💥
                </div>
                <div className="imgCardWrapper">
                  <img
                    onClick={() => {
                      window.open(elem.preview, "_blank");
                    }}
                    src={elem.imgSrc ? elem.imgSrc : badImage}
                    alt="bookImage"
                  />
                </div>
                <div className="cardAboutWrapper">
                  <div className="aboutBook">{elem.title}</div>
                  <div className="aboutAuthors">
                    <span className="authors">AUTHORS: </span>
                    {elem.authors !== undefined
                      ? elem.authors.map((elem, index) => (
                          <span key={index}>{elem}</span>
                        ))
                      : " Unknowns "}
                  </div>
                </div>
              </div>
            );
          })}
        </Modal.Body>
      </Modal>
    </>
  );
}

export default FavoritesBooks;

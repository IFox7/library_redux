// Початковий стейт редюсера
const initialState = {
  booksArr: [],
  favoritesBooksArr: [],
  modalAboutBookArr: [],
  show: false,
};
// Константи кейсів редюсера
const ADD_BOOKS = "ADD_BOOKS",
  DELETE_BOOK = "DELETE_BOOK",
  FILTER_BOOK = "FILTER_BOOK",
  ADD_FAVORITES_BOOK = "ADD_FAVORITES_BOOK",
  DELETE_FAVORITES_BOOK = "DELETE_FAVORITES_BOOK",
  MODAL_ABOUT_BOOK = "MODAL_ABOUT_BOOK",
  SEARCH_BOOK = "SEARCH_BOOK",
  SHOW = "SHOW";
// Редюсер проекту з кейсами виклику бібліотеки,улюблених книжок,фільтрації,пошуку і т.ін.
export const Reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_BOOKS:
      return { ...state, booksArr: [...action.payload] };
    case DELETE_BOOK:
      return {
        ...state,
        booksArr: state.booksArr?.filter((elem) => elem.id !== action.payload),
      };
    case SHOW:
      return { ...state, show: action.payload };
    case FILTER_BOOK:
      return {
        ...state,
        booksArr: state.booksArr.filter((elem) =>
          action.payload.name === "publishedDate"
            ? elem.volumeInfo[action.payload.name] === action.payload.value
            : elem.volumeInfo[action.payload.name] !== undefined
            ? elem.volumeInfo[action.payload.name][0] ===
              action.payload.value.split(",")[0]
            : elem
        ),
      };
    case MODAL_ABOUT_BOOK:
      return { ...state, modalAboutBookArr: [...action.payload] };
    case ADD_FAVORITES_BOOK:
      return {
        ...state,
        favoritesBooksArr: [...state.favoritesBooksArr, action.payload],
      };
    case DELETE_FAVORITES_BOOK:
      return {
        ...state,
        favoritesBooksArr: state.favoritesBooksArr?.filter(
          (elem) => elem.id !== action.payload
        ),
      };
    case SEARCH_BOOK:
      return {
        ...state,
        booksArr: state.booksArr.filter((elem) =>
          elem.volumeInfo.title
            .toLowerCase()
            .includes(action.payload.toLowerCase())
        ),
      };
    default:
      return state;
  }
};
// Екшени для виклику кейсів редюсера
export const actionAddBooks = (payload) => ({ type: ADD_BOOKS, payload });
export const actionDeleteBook = (payload) => ({ type: DELETE_BOOK, payload });
export const actionFilterBook = (payload) => ({ type: FILTER_BOOK, payload });
export const actionAddFavoritesBook = (payload) => ({
  type: ADD_FAVORITES_BOOK,
  payload,
});
export const actionDeleteFavoritesBook = (payload) => ({
  type: DELETE_FAVORITES_BOOK,
  payload,
});
export const actionModalAboutBook = (payload) => ({
  type: MODAL_ABOUT_BOOK,
  payload,
});
export const actionSearchBook = (payload) => ({ type: SEARCH_BOOK, payload });
export const actionShow = (payload) => ({ type: SHOW, payload });

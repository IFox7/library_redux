import React from "react";
import ReactDOM from "react-dom/client";

import App from "./App";
import { Provider } from "react-redux";
import { Store } from "./Store/Store";
// Основний елемент проекту
const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  // Огортаємо в провайдер ,якому передаємо стор
  <Provider store={Store}>
    <App />
  </Provider>
);

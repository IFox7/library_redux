import { actionAddBooks } from "../Store/Reducer/Reducer";
// Функція для отримання даних з API ,використовуючи redux-thunk
let currentPage = null;
export const FetchRequest = (page, next) => {
  currentPage = next === 0 ? page : currentPage + next;
  currentPage = currentPage < 0 ? 0 : currentPage;
  return (dispatch) => {
    fetch(
      `https://www.googleapis.com/books/v1/volumes?q=search-terms&startIndex=${currentPage}&maxResults=40&key=AIzaSyDWDjUC8_X7WwEOvIyXhntFt3WsDhQqeZ8`
    )
      .then((response) => response.json())
      .then((result) => {
        dispatch(actionAddBooks(result.items));
      });
  };
};

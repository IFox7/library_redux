import Modal from "react-bootstrap/Modal";

import "./bookCardModal.css";
import { actionAddFavoritesBook } from "../../Store/Reducer/Reducer";
import { useDispatch } from "react-redux";

// Модальне вікно книжки з детальною інфо про неї
function BookCardModal({ show, setShow, modal }) {
  const dispatch = useDispatch();
  return (
    <>
      <Modal show={show} fullscreen={true} onHide={() => setShow(false)}>
        <Modal.Header closeButton>
          <Modal.Title>INFORMATION ABOUT BOOK</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="bookModalWrapper">
            <div className="imgModalWrapper">
              <img src={modal.imgSrc} alt="img" />
            </div>
            <div className="aboutModalCardWrapper">
              <div className="cardModalTitle">
                <span className="aboutModalBook">Book :</span> '{modal.title}'
              </div>
              <div className="cardAuthors">
                <span className="aboutModalBook">Author :</span> {modal.authors}
              </div>
              <div className="cardModalLanguage">
                <span className="aboutModalBook">Language :</span>{" "}
                {modal.language}
              </div>
              <div className="cardModalDate">
                <span className="aboutModalBook">Published Date :</span>{" "}
                {modal.date}
              </div>
              <div className="cardModalIsbn">
                <span className="aboutModalBook">ISBN :</span> {modal.isbn}
              </div>
              <div className="cardModalDescript">
                <span className="aboutModalBook">About this book :</span>{" "}
                {modal.descript}
              </div>
              <button
                onClick={() => dispatch(actionAddFavoritesBook(modal))}
                className="btnNavbar btnFavorites"
                type="button"
              >
                Додати до обраного
              </button>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default BookCardModal;
